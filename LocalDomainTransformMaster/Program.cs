﻿using System;
using CommandLine;
using CommandLine.Text;
using MaltegoTransformNet.Core;

namespace LocalDomainTransformMaster
{
	class MainClass
	{


		public static void Main (string[] args)
		{
			//Args[0] = Command to run, comma separated
			var commands = args[0].Split(',');

			//Args[1] = Main input from maltego
			var mainInput = args[1];

			//Args[2] = Properties input from maltego
			var maltegoProperties = args.Length >= 3 
											? args[2] 
											: "";

			var dispatcher = new CommandDispatcher ();
			dispatcher.RunCommands (commands, mainInput, maltegoProperties);

		}
	}
}
