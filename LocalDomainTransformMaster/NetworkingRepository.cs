﻿using System;
using MaltegoTransformNet.Core;

namespace LocalDomainTransformMaster
{
	public class NetworkingRepository
	{
		private MaltegoResponseGenerator _maltego;
		public MaltegoResponseGenerator Maltego {
			get{ 
				if(_maltego == null){
					_maltego = new MaltegoResponseGenerator ();
				}
				return _maltego;
			}
			set{
				_maltego = value;
			}
		}

		/// <summary>
		/// Attempts to find the available remote drives from a networked computer. i.e c$,d$ etc...
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		public void ComputerToRemoteDrives(string computerName)
		{
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Attempts to find the shares running on a remote machine.
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		public void ComputerToShares(string computerName)
		{
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Attempts to locate interesting documents on the remote computer.
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		public void ComputerToInterestingDocuments(string computerName)
		{
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Attempts to locate excel documents on the remote computer.
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		public void ComputerToExcelDocuments(string computerName)
		{
			throw new NotImplementedException ();
		}
	}
}

