﻿using System;
using System.Linq;
using MaltegoTransformNet.Core;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Collections.Generic;

namespace LocalDomainTransformMaster
{
	/// <summary>
	/// Responsible for returning information from active directory
	/// </summary>
	public class ActiveDirectoryRepository
	{
		private MaltegoResponseGenerator _maltego;
		public MaltegoResponseGenerator Maltego {
			get{ 
				if(_maltego == null){
					_maltego = new MaltegoResponseGenerator ();
				}
				return _maltego;
			}
			set{
				_maltego = value;
			}
		}


		#region InternalDomainTransforms

		/// <summary>
		/// Finds computers in the domain
		/// </summary>
		/// <param name="internalDomainName">Internal domain name.</param>
		/// <param name="maltego">Maltego.</param>
		public void InternalDomainToComputers(string internalDomainName){
			using (var context = new PrincipalContext (ContextType.Domain, internalDomainName)) {
				using (var computerPrincipal = new ComputerPrincipal (context)) {
					using (var searcher = new PrincipalSearcher (computerPrincipal)) {
						foreach (ComputerPrincipal computer in searcher.FindAll ()) {
							Maltego.AddBasicEntity ("robertmcmahon.InternalComputer", computer.SamAccountName.Replace ("$", ""));
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds users in the domain.
		/// </summary>
		/// <param name="internalDomainName">Internal domain name.</param>
		/// <param name="maltego">Maltego.</param>
		public void InternalDomainToUsers(string internalDomainName){
			using (var context = new PrincipalContext (ContextType.Domain, internalDomainName)) {
				using (var userPrincipal = new UserPrincipal (context)) {
					using (var searcher = new PrincipalSearcher (userPrincipal)) {
						foreach (UserPrincipal user in searcher.FindAll ()) {
							Maltego.AddBasicEntity ("robertmcmahon.InternalUserAccount", user.SamAccountName);
						}
					}
				}
			}
		}

		///<summary>
		/// Finds all groups in the domain.
		/// </summary>
		/// <param name="internalDomainName">Domain name. Ex. domain.local</param>
		/// <param name="maltego">The MaltegoResponseGenerator object to add the entities.</param>
		public void InternalDomainToAllGroups(string internalDomainName){
			using (var context = new PrincipalContext (ContextType.Domain, internalDomainName)) {
				using (var groupPrincipal = new GroupPrincipal(context)) {
					using (var searcher = new PrincipalSearcher (groupPrincipal)) {
						foreach (GroupPrincipal group in searcher.FindAll()) {
							if (group.IsSecurityGroup.GetValueOrDefault()) {
								Maltego.AddBasicEntity ("robertmcmahon.InternalSecurityGroup", group.SamAccountName);
							} else {
								Maltego.AddBasicEntity ("robertmcmahon.InternalDistributionGroup", group.SamAccountName);
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds SecurityGroups in the domain
		/// </summary>
		/// <param name="internalDomainName">Internal domain name.</param>
		/// <param name="maltego">The MaltegoResponseGenerator object to add the entities.</param>
		public void InternalDomainToSecurityGroups(string internalDomainName){
			using (var context = new PrincipalContext (ContextType.Domain, internalDomainName)) {
				using (var groupPrincipal = new GroupPrincipal(context)) {
					using (var searcher = new PrincipalSearcher (groupPrincipal)) {
						foreach (GroupPrincipal group in searcher.FindAll()) {
							if (group.IsSecurityGroup.GetValueOrDefault()) {
								Maltego.AddBasicEntity ("robertmcmahon.InternalSecurityGroup", group.SamAccountName);
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds all DistributionGroups in the domain.
		/// </summary>
		/// <param name="internalDomainName">Internal domain name.</param>
		/// <param name="maltego">The MaltegoResponseGenerator object to add the entities.</param>
		public void InternalDomainToDistributionGroups(string internalDomainName){
			using (var context = new PrincipalContext (ContextType.Domain, internalDomainName)) {
				using (var groupPrincipal = new GroupPrincipal(context)) {
					using (var searcher = new PrincipalSearcher (groupPrincipal)) {
						foreach (GroupPrincipal group in searcher.FindAll()) {
							if (!group.IsSecurityGroup.GetValueOrDefault()) {
								Maltego.AddBasicEntity ("robertmcmahon.InternalDistributionGroup", group.SamAccountName);
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds all of the email addresses in the domain
		/// </summary>
		/// <param name="internalDomainName">Internal domain name.</param>
		/// <param name="maltego">Maltego.</param>
		public void InternalDomainToEmailAddresses(string internalDomainName){
			var directoryEntries = QueryAd ("(& (mailnickname=*)(objectClass=user))");
			foreach(var entry in directoryEntries){
				var emailAddresses = GetEmailAddressesFromDirectoryEntry (entry);
				foreach (var emailAddress in emailAddresses) {
					Maltego.AddEmailAddressEntity (emailAddress);
				}
			}
		}

		#endregion InternalDomainTransforms

		#region GroupTransforms

		/// <summary>
		/// Finds the subgroups of this group.
		/// </summary>
		/// <param name="groupName">Group name.</param>
		/// <param name="maltego">Maltego.</param>
		public void GroupToMemberGroups(string groupName){
			using(var context = new PrincipalContext(ContextType.Domain)){
				using(GroupPrincipal principal = GroupPrincipal.FindByIdentity(context, groupName)){
					foreach(GroupPrincipal subGroup in principal.GetGroups()){
						if(subGroup.IsSecurityGroup.GetValueOrDefault()){
							Maltego.AddBasicEntity ("robertmcmahon.InternalSecurityGroup", subGroup.SamAccountName);	
						}else{
							Maltego.AddBasicEntity ("robertmcmahon.InternalDistributionGroup",subGroup.SamAccountName);
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds the users in a group.
		/// </summary>
		/// <param name="groupName">Group name.</param>
		/// <param name="maltego">Maltego management object</param>
		public void GroupToUsers(string groupName){
			using (var context = new PrincipalContext (ContextType.Domain)) {
				using (GroupPrincipal principal = GroupPrincipal.FindByIdentity (context, groupName)) {
					foreach (var member in principal.GetMembers ()) {
						var userPrincipal = member as UserPrincipal;
						if (userPrincipal != null) {
							Maltego.AddBasicEntity ("robertmcmahon.InternalUserAccount", userPrincipal.SamAccountName);
						}
					}
				}
			}
		}

		/// <summary>
		/// Get's the email addresses that belong to the group.
		/// </summary>
		/// <param name="groupName">Group name.</param>
		/// <param name="maltego">Maltego.</param>
		public void GroupToEmailAddress(string groupName)
		{
			var groupDirectoryEntry = FindBySamAccountName (groupName);
			var emailAddresses = GetEmailAddressesFromDirectoryEntry (groupDirectoryEntry);
			foreach(var emailAddress in emailAddresses){
				Maltego.AddEmailAddressEntity (emailAddress);
			}
		}

		#endregion GroupTransforms

		#region UserTransforms

		/// <summary>
		/// Finds the groups that a user belongs to.
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="maltego">Maltego.</param>
		public void UserToAllGroups(string userName){
			using(var context = new PrincipalContext(ContextType.Domain)){
				using (var userPrincipal = UserPrincipal.FindByIdentity(context, userName)){
					foreach ( GroupPrincipal groupPrincipal in userPrincipal.GetGroups()){
						if(groupPrincipal.IsSecurityGroup.GetValueOrDefault()){
							Maltego.AddBasicEntity ("robertmcmahon.InternalSecurityGroup", groupPrincipal.SamAccountName);	
						}else{
							Maltego.AddBasicEntity ("robertmcmahon.InternalDistributionGroup",groupPrincipal.SamAccountName);
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds the security groups the user belongs to.
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="maltego">Maltego.</param>
		public void UserToSecurityGroups(string userName){
			using(var context = new PrincipalContext(ContextType.Domain)){
				using (var userPrincipal = UserPrincipal.FindByIdentity(context, userName)){
					foreach ( GroupPrincipal groupPrincipal in userPrincipal.GetAuthorizationGroups()){
						Maltego.AddBasicEntity ("robertmcmahon.InternalSecurityGroup", groupPrincipal.SamAccountName);
					}
				}
			}
		}

		/// <summary>
		/// Finds the distribution groups the user belongs to
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="maltego">Maltego.</param>
		public void UserToDistributionGroups(string userName){
			using(var context = new PrincipalContext(ContextType.Domain)){
				using (var userPrincipal = UserPrincipal.FindByIdentity(context, userName)){
					foreach ( GroupPrincipal groupPrincipal in userPrincipal.GetGroups()){
						if(!groupPrincipal.IsSecurityGroup.GetValueOrDefault()){
							Maltego.AddBasicEntity ("robertmcmahon.InternalDistributionGroup", groupPrincipal.SamAccountName);	
						}
					}
				}
			}
		}

		/// <summary>
		/// Finds the email address of the user
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="maltego">Maltego.</param>
		public void UserToEmailAddresses(string userName){
			var userDirectoryEntry = FindBySamAccountName (userName);
			foreach(var emailAddress in GetEmailAddressesFromDirectoryEntry(userDirectoryEntry)){
				Maltego.AddEmailAddressEntity (emailAddress);
			}
		}

		public void UserToEmailRead(string userName)
		{
			var directoryEntries = QueryAd (string.Format("(& (SAMAccountName={0})(msExchDelegateListLink=*))",userName));	
			foreach(var entry in directoryEntries){
				var delegates = entry.Properties ["msExchDelegateListLink"].Value as object[];
				foreach(var mailDelegate in delegates){
					var userNames = QueryAd (string.Format("(& (distinguishedName={0}))",mailDelegate));
					foreach(var delegateUserName in userNames){
						Maltego.AddBasicEntity ("robertmcmahon.InternalEmailRead", delegateUserName.Properties["samaccountname"][0].ToString());			
					}
				}
			}
		}

		public void UserToEmailDelegates(string userName)
		{
			var directoryEntries = QueryAd (string.Format("(& (SAMAccountName={0})(publicDelegates=*))",userName));	
			foreach(var entry in directoryEntries){
				var delegates = entry.Properties ["publicDelegates"].Value as object[];
				foreach(var mailDelegate in delegates){
					var userNames = QueryAd (string.Format("(& (distinguishedName={0}))",mailDelegate));
					foreach(var delegateUserName in userNames){
						Maltego.AddBasicEntity ("robertmcmahon.InternalEmailDelegate", delegateUserName.Properties["samaccountname"][0].ToString());			
					}
				}
			}
		}

		/// <summary>
		/// Finds the phone number for a given user
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="maltego">Maltego.</param>
		public void UserToPhoneNumbers(string userName){
			using(var context = new PrincipalContext(ContextType.Domain)){
				using(var userPrincipal = UserPrincipal.FindByIdentity(context, userName)){
					if (!string.IsNullOrWhiteSpace (userPrincipal.VoiceTelephoneNumber)) {
						Maltego.AddPhoneNumberEntity (userPrincipal.VoiceTelephoneNumber);
					}
				}
			}
		}

		#endregion UserTransforms

		#region ComputerTransforms

		public void ComputerToOperatingSystem(string computerName)
		{
			using (var context = new PrincipalContext(ContextType.Domain)){
				using(var computerPrincipal = ComputerPrincipal.FindByIdentity(context, computerName)){
					var computerDirectoryEntry = computerPrincipal.GetUnderlyingObject() as DirectoryEntry;
					if(computerDirectoryEntry != null){

					}
				}
			}	
		}

        #endregion ComputerTransforms

		#region PrivateFunctions

		private DirectoryEntry FindBySamAccountName(string samAccountName)
		{
			using(DirectorySearcher objsearch = new DirectorySearcher()){
				string strrootdse = objsearch.SearchRoot.Path ; 
				DirectoryEntry objdirentry = new DirectoryEntry(strrootdse);
				objsearch.Filter = string.Format ("(& (SAMAccountName={0}))", samAccountName);
				objsearch.SearchScope = System.DirectoryServices.SearchScope.Subtree;
				return objsearch.FindOne().GetDirectoryEntry(); 
			}
		}

		private List<string> GetEmailAddressesFromDirectoryEntry(DirectoryEntry directoryEntry)
		{
			var returnList = new List<string> ();

			var emailAddresses = directoryEntry.Properties ["proxyAddresses"].Value as object[];
			if (emailAddresses == null)
				return returnList;

			foreach(var emailAddress in emailAddresses){
				var formattedEmailAddress = emailAddress.ToString ().ToLowerInvariant ().Trim ();
				if (formattedEmailAddress.StartsWith ("smtp:")) {
					returnList.Add (formattedEmailAddress.Replace ("smtp:", ""));
				}
			} 

			return returnList;
		}

		private List<DirectoryEntry> QueryAd(string filterQuery)
		{
			var directoryEntryList = new List<DirectoryEntry> ();

			using(DirectorySearcher objsearch = new DirectorySearcher()){
				string strrootdse = objsearch.SearchRoot.Path ; 
				DirectoryEntry objdirentry = new DirectoryEntry(strrootdse);
				objsearch.Filter = filterQuery;
				objsearch.SearchScope = System.DirectoryServices.SearchScope.Subtree;
				foreach (SearchResult entry in objsearch.FindAll()){
					
					directoryEntryList.Add (entry.GetDirectoryEntry ());
				}
			}

			return directoryEntryList;
		}

		#endregion
	}
}

