﻿using System;
using MaltegoTransformNet.Core;

namespace LocalDomainTransformMaster
{
	public class SysInternalsRepository
	{
		public MaltegoResponseGenerator Maltego {
			get;
			set;
		}

		/// <summary>
		/// Attempts to return a shell from the remote computer using PsExec
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		public void ComputerToRemoteShellPsExec(string computerName, string userName, string password)
		{
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Uses PsList to get the running processes of a remote computer.
		/// </summary>
		/// <param name="computerName">Computer name.</param>
		/// <param name="maltego">Maltego.</param>
		/// <param name="userName">User name.</param>
		/// <param name="password">Password.</param>
		public void ComputerToRunningProcessesPsList(string computerName, string userName = null, string password = null)
		{
			throw new NotImplementedException ();
		}
	}
}

